import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
    padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 0, top: 15.0),
    child: Column(
      children: [
        Row(
          children: [
            Text(title),
            Expanded(child: Container()),
            Checkbox(value: value, onChanged: onChanged),
          ],
        ),
        Divider()
      ],
    ),
  );
}

class CheckBoxwidget extends StatefulWidget {
  CheckBoxwidget({Key? key}) : super(key: key);

  @override
  _CheckBoxwidgetState createState() => _CheckBoxwidgetState();
}

class _CheckBoxwidgetState extends State<CheckBoxwidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('comboBox')),
      body: ListView(
        children: [
          _checkBox('Check 1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('Check 2', check2, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('Check 3', check3, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          TextButton(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'Check1: $check1, Check2: $check2, Check3: $check3')));
              },
              child: Text('Save'))
        ],
      ),
    );
  }
}
