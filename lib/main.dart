import 'package:ui_extension/checkboxtile_widget.dart';
import 'package:ui_extension/combobox_widget.dart';
import 'package:ui_extension/dropdown_widget.dart';
import 'package:ui_extension/radio_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(title: 'Ui Extension', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ui Extension'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('Ui Menu'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('checkBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxwidget()));
              },
            ),
            ListTile(
              title: Text('checkTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTilewidget()));
              },
            ),
            ListTile(
              title: Text('Dropdown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('comboBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxwidget()));
            },
          ),
          ListTile(
            title: Text('checkTile'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTilewidget()));
            },
          ),
          ListTile(
            title: Text('Dropdown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWidget()));
            },
          ),
        ],
      ),
    );
  }
}
